package linearalgebra;
/*Tony Do 2239932*/
public class Vector3d {
    private final double x;
    private final double y;
    private final double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
    public double magnitude() {
        return Math.sqrt(x*x + y*y + z*z);
    }
    public double dotProduct(Vector3d second) {

        return (x * second.x) + (y * second.y) + (z * second.z);
    }
    public Vector3d add(Vector3d third) {
        double newX = x + third.x;
        double newY = y + third.y;
        double newZ = z + third.z;
        return new Vector3d(newX, newY, newZ);
    }
}
