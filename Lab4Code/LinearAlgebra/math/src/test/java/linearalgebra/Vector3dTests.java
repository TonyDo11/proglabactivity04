package linearalgebra;
/* Tony Do 2239932 */
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class Vector3dTests {
    
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testGetX() {
    Vector3d vector = new Vector3d (1.0,2.0,3.0);
     assertEquals(5.0, vector.getX(), 0.00001);
    }
    @Test
    public void testGetY() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);
        assertEquals(2.0, vector.getY(), 0.0001);
    }

    @Test
    public void testGetZ() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);
        assertEquals(3.0, vector.getZ(), 0.0001);
    }
    @Test
    public void testCalculateMagnitude() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);
        double expectedMagnitude = Math.sqrt(1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0);
        assertEquals(expectedMagnitude, vector.magnitude(), 0.0001);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(4.0, 5.0, 6.0);

        double expectedDotProduct = 1.0 * 4.0 + 2.0 * 5.0 + 3.0 * 6.0;
        assertEquals(expectedDotProduct, vector1.dotProduct(vector2), 0.0001);
    }

}
